<?php

class Image extends Model
{
    
    public static function resizeImage($imageName, $imageNew, $heightDest) 
    {
        list($width, $height) = getimagesize($imageName);
        $ratio = $heightDest / $height;
        $widthDest = round($width * $ratio);
        $quality = 100;
        $source = imagecreatefromjpeg($imageName);
        $dest = imagecreatetruecolor($widthDest, $heightDest);
        imagecopyresampled($dest, $source, 0, 0, 0, 0, $widthDest, $heightDest, $width, $height);
        imagejpeg($dest, $imageNew, $quality);
        imagedestroy($source);
        imagedestroy($dest);
    }

    public static function fitIntoFrame($image, $imageNew, $widthNew, $heightNew) 
    {
        list($width, $height) = getimagesize($image);
        $ratioWidth = $width / $widthNew;
        $ratioHeight = $height / $heightNew;
        $ratio = ($ratioWidth > $ratioHeight) ? $ratioWidth : $ratioHeight;
        $widthNewFit = round($width / $ratio);
        $heightNewFit = round($height / $ratio);
        $xNewFit = round(($widthNew - $widthNewFit) / 2);
        $yNewFit = round(($heightNew - $heightNewFit) / 2);
        $quality = 100;
        $source = imagecreatefromjpeg($image);
        $dest = imagecreatetruecolor($widthNew, $heightNew);
        $color = imagecolorallocate($dest, 255, 255, 255);
        imagefill($dest, 0, 0, $color);
        imagecopyresampled($dest, $source, $xNewFit, $yNewFit, 0, 0, $widthNewFit, $heightNewFit, $width, $height);
        imagejpeg($dest, $imageNew, $quality);
        imagedestroy($source);
        imagedestroy($dest);
    }
}
