<?php

class Category extends Model
{   
    
    public function __construct($category_id = 0, $category_url = '') 
    {
        $parameter = ($category_id > 0) ? $category_id : $category_url;
        $parameterName = ($category_id > 0) ? 'category_id' : 'category_url';
        if (!empty($parameter)) {
            $sql = "SELECT a.*, b.category_name as parent "
                    . "FROM categories AS a "
                    . "LEFT JOIN categories AS b "
                    . "ON a.parent_id = b.category_id "
                    . "WHERE a." . $parameterName . " =?;";
            $stmt = DataBase::handler()->prepare($sql);
            $stmt->execute([$parameter]);
            $categoryArray = $stmt->fetch();
            $this->initObjectFromArray($categoryArray);
            $this->children = self::getCategoriesByParent($this->category_id);
        }
    }   
    
    public function update() 
    {
        $sql = "UPDATE categories "
                . "SET parent_id=?, "
                . "is_active=?, "
                . "is_main=?, "
                . "category_name=?, "
                . "category_description=?, "
                . "category_long_description=?, "
                . "category_url=? "
                . "WHERE category_id=?;";
        $stmt = DataBase::handler()->prepare($sql);
        $result = $stmt->execute([
            (int)$this->parent_id,
            (int)$this->is_active,
            (int)$this->is_main,
            $this->category_name,
            $this->category_description,
            $this->category_long_description,
            $this->category_url,
            (int)$this->category_id,
        ]);
        return $result;
    }
    
    public function create() 
    {
        $sql = "INSERT INTO categories("
                . "parent_id, is_active, is_main, category_name, category_description, "
                . "category_long_description, category_url) "
                . "VALUES  (?, ?, ?, ?, ?, ?, ?);";
        $stmt = DataBase::handler()->prepare($sql);
        $result = $stmt->execute([
            (int)$this->parent_id,
            (int)$this->is_active,
            (int)$this->is_main,
            $this->category_name,
            $this->category_description,
            $this->category_long_description,
            $this->category_url,
        ]);
        $this->category_id = DataBase::handler()->lastInsertId();
        return $result;
    }

    public static function getCategories() 
    {
        $sql = "SELECT * FROM categories WHERE is_active=1 ORDER BY sort_order ASC;";
        $stmt = DataBase::handler()->query($sql);
        return $stmt->fetchAll();
    }

    public static function getCategoriesMenu() 
    {
        $parents = self::getCategoriesByParent(0);
        foreach ($parents as &$parent){
            $parent["children"] = self::getCategoriesByParent($parent["category_id"]);
        }
        return $parents;
    }

    public static function getCategoriesByParent($parent_id) 
    {
        $sql = "SELECT * FROM categories WHERE (is_active=1 AND parent_id=?) ORDER BY sort_order ASC;";
        $stmt = DataBase::handler()->prepare($sql);
        $stmt->execute([$parent_id]);
        return $stmt->fetchAll();
    }
    
    public static function getCategoriesAdmin() 
    {
        $sql = "SELECT a.*, b.category_name as parent FROM categories AS a "
                . "LEFT JOIN categories AS b ON a.parent_id = b.category_id;";
        $stmt = DataBase::handler()->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll();
    }
   
}