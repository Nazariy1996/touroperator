<?php
class Country extends Model
{   

    public static function getCountries($isActive = 1)
    {
        
        $sql = "SELECT * FROM countries WHERE is_active=?";
        $stmt = DataBase::handler()->prepare($sql);
        $stmt->execute([$isActive]);
        return $stmt->fetchAll();
    }
}