<?php
class Tour extends Model
{   
    public $file_folder = '';
    public $tour_prefix = '';
    public $background_image;
    public $background_image_header;
    public $galleryIDs = [];
    public $galleries = [];
    public $categoryIDs = [];
    public $categories = [];
    public $meta_title = '';
    
    function __construct($tour_id = 0) 
    {
        if ($tour_id>0) {
            $sql = "SELECT * FROM tours WHERE tour_id=?;";
            $stmt = DataBase::handler()->prepare($sql);
            $stmt->execute([(int)$tour_id]);
            $tourArray = $stmt->fetch();
            $this->initObjectFromArray($tourArray);
            $this->meta_title = SITE_NAME . " - " . $this->tour_name;
            $this->galleryIDs = $this->getGalleryIDs($this->tour_id);
            $this->categoryIDs = $this->getCategoryIDs($this->tour_id);
            
        }
    }
    
    public function update() 
    {
        $sql = "UPDATE tours SET is_active=?, tour_name=?, "
                . "tour_description=?, tour_info=?, "
                . "tour_price=?, tour_length_id=?, "
                . "meta_description=?, meta_keywords=? "
                . "WHERE tour_id=?;";
        $stmt = DataBase::handler()->prepare($sql);
        $result = $stmt->execute([
            (int)$this->is_active,
            $this->tour_name,
            $this->tour_description,
            $this->tour_info,
            $this->tour_price,
            (int)$this->tour_length_id,
            $this->meta_description,
            $this->meta_keywords,
            (int)$this->tour_id,
        ]);
        if ($result) {
            $stmt = DataBase::handler()->prepare("DELETE FROM tour_gallery WHERE tour_id=?;");
            $stmt->execute([(int)$this->tour_id]);
            $stmt = DataBase::handler()->prepare("DELETE FROM tour_category WHERE tour_id=?;");
            $stmt->execute([(int)$this->tour_id]);

            $sql = "INSERT INTO tour_gallery(tour_id, gallery_id) VALUES (?, ?);";
            $stmt = DataBase::handler()->prepare($sql);
            foreach($this->galleryIDs as $gallery_id) {
                $stmt->execute([
                    (int)$this->tour_id,
                    $gallery_id,
                ]);
            }
            
            $sql = "INSERT INTO tour_category(tour_id, category_id) VALUES (?, ?);";
            $stmt = DataBase::handler()->prepare($sql);
            foreach($this->categoryIDs as $category_id) {
                $stmt->execute([
                    (int)$this->tour_id,
                    $category_id,
                ]);
            }
            
            
            return true;
        } else {
            return false;
        }
    }
    
    public function create() 
    {
        $sql = "INSERT INTO tours"
                . "(is_active, tour_name, tour_description, "
                . "tour_info, tour_price, "
                . "tour_length_id, meta_description, meta_keywords) "
                . "VALUES  (?, ?, ?, ?, ?, ?, ?, ?);";
        $stmt = DataBase::handler()->prepare($sql);
        $result = $stmt->execute([
            (int)$this->is_active,
            $this->tour_name,
            $this->tour_description,
            $this->tour_info,
            $this->tour_price,
            (int)$this->tour_length_id,
            $this->meta_description,
            $this->meta_keywords
        ]);
        $this->tour_id = DataBase::handler()->lastInsertId();
        if ($result) {
            $sql = "INSERT INTO tour_gallery(tour_id, gallery_id) VALUES (?, ?);";
            $stmt = DataBase::handler()->prepare($sql);
            foreach($this->galleryIDs as $gallery_id) {
                $stmt->execute([
                    (int)$this->tour_id,
                    $gallery_id,
                ]);
            }
            
            $sql = "INSERT INTO tour_category(tour_id, category_id) VALUES (?, ?);";
            $stmt = DataBase::handler()->prepare($sql);
            foreach($this->categoryIDs as $category_id) {
                $stmt->execute([
                    (int)$this->tour_id,
                    $category_id,
                ]);
            }
            return true;
        } else {
            return false;
        }
    }
    
    private function getGalleryIDs($tour_id) 
    {
        $sql="SELECT gallery_id FROM tour_gallery WHERE tour_id=?;";
        $stmt = DataBase::handler()->prepare($sql);
        $stmt->execute([(int)$tour_id]);
        $galleries = $stmt->fetchAll();
        $result = [];
        foreach($galleries as $key => $gallery){
            $result[$gallery['gallery_id']]=$gallery['gallery_id'];
        }
        return $result;
    }
    
    public function setGalleryIDs($galleryIDs)
    {
        $result = [];
        if (!empty($galleryIDs)) {
            foreach($galleryIDs as $gallery_id) {
                $result[$gallery_id] = $gallery_id;
            }
        }
        $this->galleryIDs = $result;
    }
    
    public function getGalleries($tour_id)
    {
        $sql = "SELECT * FROM tour_gallery AS a "
                . "LEFT JOIN galleries AS b "
                . "ON a.gallery_id=b.gallery_id "
                . "WHERE a.tour_id=? ORDER BY a.gallery_id";
        $stmt = DataBase::handler()->prepare($sql);
        $stmt->execute([(int)$tour_id]);
        $galleries = $stmt->fetchAll();
        $galleryObjects = [];
        foreach ($galleries as $key => $gallery) {
            $galleryObjects[$key] = new Gallery($gallery['gallery_id']);
            $galleryObjects[$key]->getInfoById($gallery['gallery_id']);
            $galleryObjects[$key]->getImages();
        }
        return $galleryObjects;
    }

    private function getCategoryIDs($tour_id) 
    {
        $sql="SELECT category_id FROM tour_category WHERE tour_id=?;";
        $stmt = DataBase::handler()->prepare($sql);
        $stmt->execute([(int)$tour_id]);
        $categories = $stmt->fetchAll();
        $result = [];
        foreach($categories as $key => $category){
            $result[$category['category_id']]=$category['category_id'];
        }
        return $result;
    }
    
    public function setCategoryIDs($categoryIDs)
    {
        $result = [];
        if (!empty($categoryIDs)) {
            foreach($categoryIDs as $category_id) {
                $result[$category_id] = $category_id;
            }
        }
        $this->categoryIDs = $result;
    }
    
    public function getCategories($tour_id)
    {
        $sql = "SELECT * FROM tour_category AS a "
                . "LEFT JOIN categories AS b "
                . "ON a.category_id=b.category_id "
                . "WHERE a.tour_id=? ORDER BY a.category_id";
        $stmt = DataBase::handler()->prepare($sql);
        $stmt->execute([(int)$tour_id]);
        return $stmt->fetchAll();
    }    
  
    public static function getToursByCategory($category_id, $tour_length_id = 0, $is_active = 1)
    {
        $where = [];
        $where[] = "a.category_id=" . (int)$category_id;
        if ($tour_length_id > 0) {
            $where[] = "b.tour_length_id=" . (int)$tour_length_id;
        }
        if ($is_active >= 0) {
            $where[] = "b.is_active=" . (int)$is_active;
        }
        $whereSQL = "";
        if (!empty($where)) {
            $whereSQL = " WHERE (" . join(" AND ", $where) . ")";
        }
        
        $sql = "SELECT a.*, b.*, c.category_name, d.* FROM tour_category AS a "
                . "LEFT JOIN tours AS b ON a.tour_id=b.tour_id "
                . "LEFT JOIN categories AS c ON a.category_id=c.category_id "
                . "LEFT JOIN lengths AS d ON b.tour_length_id=d.tour_length_id "
                . $whereSQL." ORDER BY a.category_id, b.sort_order ASC";
        
        $stmt = DataBase::handler()->prepare($sql);
        $stmt->execute();
        $tours = $stmt->fetchAll();
        foreach ($tours as &$tourItem) {
          $tourItem['categories'] = self::getCategoriesByTour($tourItem['tour_id']);
        }
        return $tours;      
    }
    
    public static function getTours($tour_length_id = 0, $is_active = 1)
    {
        $where = [];
        if ($tour_length_id > 0) {
            $where[] = "a.tour_length_id=" . (int)$tour_length_id;
        }
        if ($is_active >= 0) {
            $where[] = "a.is_active=" . (int)$is_active;
        }
        $whereSQL = "";
        if (!empty($where)) {
            $whereSQL = " WHERE (" . join(" AND ", $where) . ")";
        }
        
        $sql = "SELECT a.*, b.* FROM tours AS a "
                . "LEFT JOIN lengths AS b ON a.tour_length_id=b.tour_length_id "
                . $whereSQL." ORDER BY a.tour_id, a.sort_order ASC";
        
        $stmt = DataBase::handler()->prepare($sql);
        $stmt->execute();
        $tours = $stmt->fetchAll();
        foreach ($tours as &$tourItem) {
          $tourItem['categories'] = self::getCategoriesByTour($tourItem['tour_id']);
        }
        return $tours;
    }
    
    public static function getTourLengths($tour_length_id = 0) 
    {
        $where = '';
        if ($tour_length_id>0) {
            $where=" WHERE tour_length_id=" . $tour_length_id;
        }
        $sql = "SELECT * FROM lengths ".$where;
        $stmt = DataBase::handler()->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll();
    }
    
    private static function getCategoriesByTour($tour_id)
    {
      $sql = "SELECT a.*, b.category_name FROM tour_category AS a "
              . "LEFT JOIN categories AS b ON a.category_id=b.category_id "
              . "WHERE a.tour_id=?";
      $stmt = DataBase::handler()->prepare($sql);
      $stmt->execute([(int)$tour_id]);
      return $stmt->fetchAll();
    }
    
}