<?php

class Gallery extends Model
{
    public $img_folder = '';
    public $prefix_folder = '';
    public $gallery_folder = '';
    public $gallery_folder_out = '';
    public $gallery_uploads = [];
    public $gallery_images = [];

    /**
    * Initializes a Gallery
    * Modified 06.07.2017 by NazarKrobust
    */
    public function __construct($gallery_id = 0)
    {
        $this->gallery_id = $gallery_id;
        if ($gallery_id > 0) {
            $this->img_folder = 'images/galleries';
            $this->prefix_folder = 'gallery_';
            $this->gallery_folder = $this->img_folder . '/' . $this->prefix_folder . $this->gallery_id;
        }
    }
    
    /**
    * Gets full information about the Gallery
    * Modified 06.07.2017 by NazarKrobust
    */
    public function getInfoById($gallery_id) 
    {
        $sql = "SELECT * FROM galleries WHERE gallery_id=?;";
        $stmt = DataBase::handler()->prepare($sql);
        $stmt->execute([$gallery_id]);
        $galleryArray = $stmt->fetch();
        $this->initObjectFromArray($galleryArray);
    }

    /**
     * Updates the Gallery
     * Modified 06.07.2017 by NazarKrobust
     */
    public function update()
    {
        $sql = "UPDATE galleries SET gallery_name=?, gallery_description=? WHERE gallery_id=?";
        $stmt = DataBase::handler()->prepare($sql);
        $result = $stmt->execute([
            $this->gallery_name,
            $this->gallery_description,
            $this->gallery_id,
        ]);
        $this->uploadImages();
    }
    
    /**
     * Creates a new Gallery
     * Modified 06.07.2017 by NazarKrobust
     */
    public function create() 
    {
        $sql = "INSERT INTO galleries (gallery_name, gallery_description) "
                . "VALUES  (?, ?);";
        $stmt = DataBase::handler()->prepare($sql);
        $stmt->execute([
            $this->gallery_name,
            $this->gallery_description,
        ]);
        
        $this->gallery_id = DataBase::handler()->lastInsertId();
        $this->gallery_folder = $this->img_folder . '/' . $this->prefix_folder . $this->gallery_id;
        FileSystem::makeFolder($this->gallery_folder);
        FileSystem::makeFolder($this->gallery_folder . "/600x400");
        FileSystem::makeFolder($this->gallery_folder . "/thumb");
    }

    /**
     * Deletes the Gallery 
     * if it has no Images
     * Modified 06.07.2017 by NazarKrobust
     */   
    public function delete() 
    {
        if ($this->imagesCount() == 0) {
            $sql = "DELETE FROM galleries WHERE gallery_id=?";
            $stmt = DataBase::handler()->prepare($sql);
            $result = $stmt->execute([$this->gallery_id]);
            FileSystem::deleteFolder($this->gallery_folder."/thumb");
            FileSystem::deleteFolder($this->gallery_folder."/600x400");
            FileSystem::deleteFolder($this->gallery_folder);
        }
    }
    
    /**
     * Returns the list of Galleries 
     * with Image Counts
     * Modified 06.07.2017 by NazarKrobust
     */      
    public static function getGalleries() 
    {
        $sql = "SELECT a.*, COUNT(b.image_id) AS image_count "
                . "FROM galleries AS a "
                . "LEFT JOIN images AS b "
                . "ON a.gallery_id=b.gallery_id "
                . "GROUP BY a.gallery_id "
                . "ORDER BY a.gallery_id;";
        $stmt = DataBase::handler()->prepare($sql);
        $stmt->execute();
        $galleries = $stmt->fetchAll();
        return $galleries;
    }
    
    /**
     * Counts Images in the Gallery
     * Modified 06.07.2017 by NazarKrobust
     */     
    public function imagesCount()
    {
        $sql = "SELECT image_id FROM images WHERE gallery_id=?";
        $stmt = DataBase::handler()->prepare($sql);
        $stmt->execute([$this->gallery_id]);
        return $stmt->rowCount();
    }
    
    /**
     * Upload Images to Gallery
     * Modified 06.07.2017 by NazarKrobust
     */
    private function uploadImages()
    {
        $uploads = $_FILES;
        $sql = "INSERT INTO images(image, gallery_id, sort_order) VALUES (?, ?, 0);";
        $stmt = DataBase::handler()->prepare($sql);
        foreach ($uploads['image']['tmp_name'] as $key => $imageTemp) {
            $types = ['image/jpeg','image/pjpeg'];
            if (in_array($uploads['image']['type'][$key], $types)) {
                FileSystem::makeFolder($this->gallery_folder);
                FileSystem::makeFolder($this->gallery_folder . "/600x400");
                FileSystem::makeFolder($this->gallery_folder . "/thumb");
                $image = md5(microtime() . rand(100000, 999999) . $imageTemp) . ".jpg";
                $imageFull = $this->gallery_folder . "/" . $image;
                if (copy($imageTemp, $imageFull)) {
                    list($width, $height) = getimagesize($imageFull);
                    if ($height > 600) {
                        Image::resizeImage($imageFull, $imageFull, 600);
                    }
                    Image::resizeImage($imageFull, $this->gallery_folder . "/thumb/" . $image, 70);
                    Image::fitIntoFrame($imageFull, $this->gallery_folder . "/600x400/" . $image, 600, 400);
                    
                    $result = $stmt->execute([
                        $image, 
                        $this->gallery_id,
                    ]);
                }
            }
        }
    }

    /**
     * Get the Images for Gallery
     * Modified 06.07.2017 by NazarKrobust
     */
    public function getImages() 
    {
        $sql = 'SELECT * FROM images WHERE gallery_id=? ORDER BY sort_order';
        $stmt = DataBase::handler()->prepare($sql);
        $stmt->execute([$this->gallery_id]);
        $images = $stmt->fetchAll();
        $folder = $this->gallery_folder;
        foreach($images as &$image){
            $image['image_full'] = '/' . $folder . '/' . $image['image'];
            $image['image_600x400'] = '/' . $folder . '/600x400/' . $image['image'];
            $image['small'] = '/' . $folder . '/thumb/' . $image['image'];
        }
        $this->gallery_images = $images;
    }

   
    /**
    * Deletes the Images of the Gallery
    * Modified 06.07.2017 by NazarKrobust
    */    
    public function deleteImages($images) 
    {
        if (!empty($images)) {
            $folder = $this->gallery_folder;
            if (is_dir($folder)) {
                foreach ($images as $image) {
                    FileSystem::deleteFile($folder.'/'.$image);
                    FileSystem::deleteFile($folder.'/thumb/'.$image);
                    FileSystem::deleteFile($folder.'/600x400/'.$image);
                }
            }
            $sql = "DELETE FROM images WHERE image=?;";
            $stmt = DataBase::handler()->prepare($sql);
            foreach($images as $image) {
                $result = $stmt->execute([$image]);
                if (!$result) {
                    return false; 
                }
             }
        }
        return true;
    }    
    

  
    /**
     * Updates Image Title
     * Modified 06.07.2017 by NazarKrobust
     */     
    public static function saveImageTitle($imageID, $imageTitle) 
    {
        $sql = "UPDATE images SET title=? WHERE image_id=?";
        $stmt = DataBase::handler()->prepare($sql);
        $result = $stmt->execute([$imageTitle, $imageID]);
        return $result;
    }
}

?>
