<div class="container">
  <div class="card-panel">
    <form class="col s6 offset-s3" action="/admin/login/" method="POST">
      <div class="input-field col l10 m12 s12">
        <i class="material-icons prefix">account_circle</i>
        <input id="name" name="name" type="text" class="validate" required="required">
        <label for="name" data-error="Помилка" data-success="Правильно">Ім'я</label>
      </div>
      <div class="input-field col l10 m12 s12">
        <i class="material-icons prefix">lock</i>
        <input id="password" name="password" type="password" class="validate" required="required"  pattern="[^\s]{4,20}">
        <label for="password" data-error="Помилка" data-success="Правильно">Пароль</label>
      </div>
      <button class="btn waves-effect waves-light" type="submit" name="action">Увійти
        <i class="material-icons right">send</i>
      </button>
    </form>
  </div>
</div>