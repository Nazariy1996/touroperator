
<div class="container">
  <div class="section">
    <h5>Система "Туристичний оператор"</h5>
    <p class="justify-align light"><strong>Веб-система "Туристичний оператор"</strong> пропонує функціонал для створення сайту туристичної агенції, з можливістю керування турами, категоріями турів, зображеннями та галереями зображень.</p>

  </div>
  <section id="main-categories">
    <h5>Категорії турів</h5>
    <div class="row">

      <?php foreach ($categories as $key => $category): ?>
        <?php if ($category["is_main"] == 1): ?>
          <div class="col s12 m6 l4" id="categories">
            <div class="card category-item">
              <div class="card-image">
                <a href="/tours/list/<?php echo $category["category_url"] ?>">
                  <img src="images/categories/<?php echo $category["category_id"] ?>.jpg">
                </a>
              </div>
              <div class="card-action">
                <a class="teal-text" href="/tours/list/<?php echo $category["category_url"] ?>"><?php echo $category["category_name"] ?></a>
              </div>
            </div>
          </div>
        <?php endif; ?> 
      <?php endforeach; ?>
    </div>
  </section>

</div>


<div class="container">
  <div class="section">
    <div class="row">
      <div class="col s12">
        <h3><i class="mdi-content-send brown-text"></i></h3>
        <h4>Ми пропонуємо</h4>
        <ul class="collection">
          <li class="collection-item"><i class="material-icons secondary-content">check box</i>Автобусні тури по Україні для дорослих та учнівські екскурсії</li>
          <li class="collection-item"><i class="material-icons secondary-content">check box</i>Якісний відпочинок на морі</li>
          <li class="collection-item"><i class="material-icons secondary-content">check box</i>Високий рівень транспортного обслуговування</li>
          <li class="collection-item"><i class="material-icons secondary-content">check box</i>Корпоративне обслуговування</li>
          <li class="collection-item"><i class="material-icons secondary-content">check box</i>Висококваліфікованих екскурсоводів</li>
          <li class="collection-item"><i class="material-icons secondary-content">check box</i>Допомогу в оформленні необхідних документів для екскурсії</li>
        </ul>
      </div>
    </div>
  </div>
</div>