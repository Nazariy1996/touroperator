<div class="container">
  <nav class="breadcrumbs">
    <div class="nav-wrapper">
      <div class="col s12">
        <?php foreach($breadcrumbs as $key => $item): ?>
          <a href="<?= $item["url"]; ?>" class="breadcrumb"><?= $item["title"]; ?></a>
        <?php endforeach; ?>
      </div>
    </div>
  </nav>

  <div class="row">

    <div class="col s12 l3">
      <div id="leftsidemenu" class="collection with-header">
        <div class="collection-header"><h5>Категорії турів</h5></div>
        <?php foreach($menu as $key => $menuItem): ?>
          <?php if (!$menuItem["children"]): ?>
            <a class="collection-item waves-effect waves-teal <?php if ($menuItem['category_id'] == $category->category_id): ?>active<?php endif; ?>"  href="/tours/list/<?php echo $menuItem["category_url"]?>">
              <?php echo $menuItem["category_name"]?>
            </a>
          <?php else: ?>
            <ul class="collapsible" data-collapsible="accordion">
              <li>
                <div class="collapsible-header">
                  <a class="collection-item waves-effect waves-teal"><?php echo $menuItem["category_name"]?> <i class="material-icons right">arrow_drop_down</i></a>
                </div>
                <div class="collapsible-body">
                  <ul>
                    <?php foreach($menuItem["children"] as $key => $child): ?>
                      <li><a class="collection-item <?php if ($child['category_id'] == $category->category_id): ?>active<?php endif; ?>" href="/tours/list/<?php echo $child["category_url"]?>"><?php echo $child["category_name"]?></a></li>
                    <?php endforeach;?> 
                  </ul>
                </div>
              </li>
            </ul>
          <?php endif; ?>
        <?php endforeach;?> 
      </div>          
    </div>

    <div class="col s12 l2 push-l7">
      <div class="card-panel">    
        <form action="/tours/list/<?= $category->category_url; ?>" method="POST">
          <div class="input-field">

            <select name="tour_length_id" onchange="javascript: this.form.submit();">
              <option value="0">Усі</option>
              <?php foreach($tour_lengths as $key => $tour_length): ?>
                <?php if ($tour_length_id == $tour_length["tour_length_id"]): ?>
                  <option value="<?= $tour_length["tour_length_id"]; ?>" selected><?= $tour_length["tour_length"]; ?></option>
                <?php else: ?>
                  <option value="<?= $tour_length["tour_length_id"]; ?>"><?= $tour_length["tour_length"]; ?></option>
                <?php endif; ?>
              <?php endforeach; ?>
            </select>
            <label>Виберіть тривалість:</label>
          </div>
        </form>
      </div>            
    </div>

    <div class="col s12 l7 pull-l2">        
      <div class="card-panel justify-align">
        <h4><?= $category->category_name; ?></h4>
        <?= $category->category_long_description; ?>
        <?php if ($category->children): ?>
          <div class="row">
            <?php foreach($category->children as $child): ?>
              <div class="col s12 m6 l4" id="categories">
                <div class="card category-item">
                  <div class="card-image">
                    <img src="/images/categories/<?php echo $child['category_id']?>.jpg">
                  </div>
                  <div class="card-content">
                    <p><a href="/tours/list/<?php echo $child['category_url']?>"><?php echo $child['category_name']?></a></p>
                  </div>
                </div>
              </div>
            <?php endforeach;?>
          </div>
        <?php endif; ?>

        <div class="collection">
          <?php foreach($tours as $tour): ?>
            <a class="collection-item avatar" href="/tour/<?= $category->category_id; ?>/<?= $tour['tour_id']; ?>">
              <img class="circle" src="/images/pictograms/<?= $tour['category_id']; ?>.png">
              <span class="title"><?= $tour['tour_name']; ?></span>
              <p>Тривалість: <?= $tour['tour_length']; ?></p>
            </a>
          <?php endforeach; ?>
        </div>
        <?php if (count($tours) > 10): ?>
          <div class="center">
            <a class="btn-floating btn waves-effect waves-light blue" href="#0"><i class="material-icons">arrow_drop_up</i></a>
          </div>
        <?php endif; ?>
      </div> 
    </div>
  </div>
</div>
