<div class="container">
  <nav class="breadcrumbs">
    <div class="nav-wrapper">
      <div class="col s12">
        <?php foreach($breadcrumbs as $key => $item): ?>
          <a href="<?= $item["url"]; ?>" class="breadcrumb"><?= $item["title"]; ?></a>
        <?php endforeach; ?>
      </div>
    </div>
  </nav>
  <h4><?= $tour->tour_name; ?></h4>
  <div class="card-panel">
    <div class="row">
      <div class="col s12">
        <div class="article">
          <?= $tour->tour_info; ?>
        </div>
      </div>
    </div>
    <?php if($tour->galleries): ?>
      <?php foreach($tour->galleries as $key => $gallery): ?>
        <div class="row">
          <div class="col s12 m12 l8 offset-l2">
            <h5><?= $gallery->gallery_name; ?></h5>
            <ul class="bxslider bxslider<?= $key ?>">
              <?php foreach($gallery->gallery_images as $key1 => $image): ?>
                <li><img src="<?= $image["image_600x400"]; ?>" title="<?= $image["title"]; ?>"></li>
              <?php endforeach;?>
            </ul>
            <ul class="bxslider bxslider-th-<?= $key ?>" id="bx-pager-<?= $key ?>">
              <?php $i=0; ?>
              <?php foreach($gallery->gallery_images as $key1 => $image): ?>
                <li><a data-slide-index="<?= $i++; ?>" href=""><img src="<?= $image["small"]; ?>"></a></li>
              <?php endforeach;?>
            </ul>
            <script>
              $(document).ready(function(){
                $('.bxslider<?= $key ?>').bxSlider({
                  mode: "fade",
                  captions: true,
                  pager: true,
                  slideMargin: 0,
                  pagerCustom: '#bx-pager-<?= $key ?>'
                });
                $('.bxslider-th-<?= $key ?>').bxSlider({
                  minSlides: 1,
                  maxSlides: 10,
                  slideWidth: 100,
                  slideMargin: 10
                });
                $(".bx-wrapper").css("margin-bottom", "10px");
              });
            </script>
          </div>
        </div>
      <?php endforeach;?>
    <?php endif;?>

    <div class="row teal-text text-lighten-2">
      <div class="col s12">
        <?php if($tour->tour_price) echo $tour->tour_price; ?>
      </div>
    </div>
  </div>
</div>
