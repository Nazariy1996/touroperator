<div class="container">   
  <div class="card-panel">    
    <form action="/admin/tours/" method="POST">
      <div class="row">
        <div class="col s12 m4">
          <label for="category_id">Категорія</label>
          <select name="category_id" onchange="javascript: this.form.submit();">
            <option value="0">Всі тури</option>
            <?php foreach($categories as $key => $category): ?>
              <?php if ($category_id == $category["category_id"]): ?>
                <option value="<?= $category["category_id"]; ?>" selected><?= $category["category_name"]; ?></option>
              <?php else: ?>
                <option value="<?= $category["category_id"]; ?>"><?= $category["category_name"]; ?></option>
              <?php endif; ?>
            <?php endforeach; ?>
          </select>
        </div>
        <div class="col s12 m4">
          <label for="tour_length_id">Тривалість</label>
          <select name="tour_length_id" onchange="javascript: this.form.submit();">
            <option value="0">Всі тури</option>
            <?php foreach($tour_lengths as $key => $tour_length): ?>
              <?php if ($tour_length_id == $tour_length["tour_length_id"]): ?>
                <option value="<?= $tour_length["tour_length_id"]; ?>" selected><?= $tour_length["tour_length"]; ?></option>
              <?php else: ?>
                <option value="<?= $tour_length["tour_length_id"]; ?>"><?= $tour_length["tour_length"]; ?></option>
              <?php endif; ?>
            <?php endforeach; ?>
          </select>
        </div>
        <div class="col s12 m4">
          <label for="is_active">Статус</label>
          <select name="is_active" id="is_active" onchange="javascript: this.form.submit();">
            <option value="-1" <?php if ($is_active == -1): ?>selected<?php endif; ?>>Усі</option>
            <option value="1" <?php if ($is_active == 1): ?>selected<?php endif; ?>>Активні</option>
            <option value="0" <?php if ($is_active == 0): ?>selected<?php endif; ?>>Не активні</option>
          </select>
        </div>
      </div>
    </form>

    <table>
      <tr>
        <th>ID туру</th>
        <th>Назва туру</th>
        <th>Категорія</th>
      </tr>        
      <?php foreach($tours as $tourItem): ?>
        <tr>
          <td><?= $tourItem['tour_id']; ?></td>
          <td>
            <a href="/admin/tour/edit/<?= $tourItem['tour_id']; ?>">
              <span <?php if ($tourItem["is_active"]==0) echo "class='red-text line-through'" ?>>
                <?= $tourItem['tour_name']; ?>
              </span>
            </a>
          </td>
          <td>
            <?php foreach($tourItem['categories'] as $categoryItem): ?>
            <?= $categoryItem['category_name'] ?><br>
            <?php endforeach; ?>
            
          </td>
        </tr>       
      <?php endforeach; ?>
    </table>
  </div>
</div>
