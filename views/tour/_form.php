<div class="container">
  <div class="card-panel">
    <form class="col s10 offset-s1" action="/admin/tours" method="POST">

      <div class="input-field col l10 m12 s12">
        <i class="material-icons prefix">menu</i>
        <select id="tour_length_id" name="tour_length_id">
          <?php foreach($tour_lengths as $key => $tour_length): ?>
            <?php if ($tour->tour_length_id == $tour_length["tour_length_id"]): ?>
              <option value="<?= $tour_length["tour_length_id"]; ?>" selected><?= $tour_length["tour_length"]; ?></option>
            <?php else: ?>
              <option value="<?= $tour_length["tour_length_id"]; ?>"><?= $tour_length["tour_length"]; ?></option>
            <?php endif; ?>
          <?php endforeach; ?>
        </select>
        <label for="tour_length_id">Тривалість</label>
      </div>

      <div class="input-field col l10 m12 s12">
        <i class="material-icons prefix">menu</i>
        <input id="tour_id" name="tour_id" type="text" readonly="readonly" value="<?= $tour->tour_id; ?>">
        <label for="tour_id">ID туру</label>
      </div>

      <div class="input-field col l10 m12 s12">
        <i class="material-icons prefix">menu</i>
        <input id="tour_name" name="tour_name" type="text" required="required" value="<?php echo $tour->tour_name; ?>">
        <label for="tour_name">Назва</label>
      </div>

      <div class="row">
        <div class="input-field col l10 m12 s12" style="text-align: left; margin-left: 8px;">
          <label for="tour_description">Короткий опис:</label>
        </div>
        <div class="input-field col l10 m12 s12" style="margin: 50px 50px;">
          <textarea id="tour_description" name="tour_description"><?php echo $tour->tour_description; ?></textarea>
          <script>
            CKEDITOR.replace( 'tour_description' , {
              allowedContent: true
            });
          </script>
        </div>
      </div>

      <div class="row">
        <div class="input-field col l10 m12 s12" style="text-align: left; margin-left: 8px;">
          <label for="tour_info">Повний опис:</label>
        </div>
        <div class="input-field col l10 m12 s12" style="margin: 50px 50px;">
          <textarea id="tour_info" name="tour_info"><?php echo $tour->tour_info; ?></textarea>
          <script>
            CKEDITOR.replace( 'tour_info' , {
              allowedContent: true
            });
          </script>
        </div>
      </div>

      <div class="input-field col l10 m12 s12">
        <i class="material-icons prefix">menu</i>
        <input id="tour_price" name="tour_price" type="text" value="<?= $tour->tour_price ?>">
        <label for="tour_price">Ціна</label>
      </div>

      <div class="row">
        <div class="col s12">
          <ul class="tabs">
            <li class="tab col s6"><a class="active" href="#tabCategories">Категорії</a></li>
            <li class="tab col s6"><a href="#tabGalleries">Галереї</a></li>
          </ul>
        </div>
        <div class="col s12" id="tabCategories">
          <h5>Категорії</h5>

          <ul class="collection">
            <?php foreach($categories as $key => $categoryItem): ?>
              <li class="collection-item">
                <label>
                  <input type="checkbox" name="tour_category[]" value="<?= $categoryItem["category_id"]; ?>" <?php if ($tour->categoryIDs[$categoryItem["category_id"]]): ?>checked<?php endif; ?>>
                  <span><?= $categoryItem["category_name"]; ?></span>
                </label>
              </li>
            <?php endforeach; ?>
          </ul>   
        </div>
        <div class="col s12" id="tabGalleries">
          <h5>Галереї</h5>
          <ul class="collection">
            <?php foreach($galleries as $key => $gallery): ?>
              <li class="collection-item">
                <label>
                  <input type="checkbox" name="tour_gallery[]" value="<?= $gallery["gallery_id"]; ?>" <?php if ($tour->galleryIDs[$gallery["gallery_id"]]): ?>checked<?php endif; ?>>
                  <span><?= $gallery["gallery_name"]; ?></span>
                <label>
              </li>
            <?php endforeach; ?>
          </ul>          
        </div>

      </div>

      <div class="col l10 m12 s12">
        <label>
          <input name="is_active" type="checkbox" value='1' <?php if ($tour->is_active): ?>checked<?php endif; ?>>
          <span>Активний</span>
        </label>
      </div>

      <div class="input-field col l10 m12 s12">
        <i class="material-icons prefix">menu</i>
        <input id="meta_description" name="meta_description" type="text" value="<?= $tour->meta_description ?>">
        <label for="meta_description">META description</label>
      </div>

      <div class="input-field col l10 m12 s12">
        <i class="material-icons prefix">menu</i>
        <input id="meta_keywords" name="meta_keywords" type="text" value="<?= $tour->meta_keywords ?>">
        <label for="meta_keywords">META keywords</label>
      </div>   

      <div class="input-field col l10 m12 s12 center-align">
        <?php if ($tour): ?> 
          <input type="hidden" name="action" value="update_tour">
          <button class="btn waves-effect waves-light" type="submit">
            Змінити<i class="material-icons right">send</i>
          </button>
        <?php else: ?>
          <input type="hidden" name="action" value="add_tour">
          <button class="btn waves-effect waves-light" type="submit">
            Додати<i class="material-icons right">send</i>
          </button>
        <?php endif; ?> 
      </div>

    </form>
  </div>
</div>
