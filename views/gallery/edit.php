<div class="container">
  <div class="card-panel">
    <form class="col s10 offset-s1" action="/admin/gallery/edit/<?= $gallery->gallery_id ?>" method="POST" enctype='multipart/form-data'>

      <div class="input-field col l10 m12 s12">
        <i class="material-icons prefix">menu</i>
        <input id="gallery_id" name="gallery_id" type="text" readonly="readonly" value="<?= $gallery->gallery_id; ?>">
        <label for="gallery_id">ID галереї</label>
      </div>

      <div class="input-field col l10 m12 s12">
        <i class="material-icons prefix">menu</i>
        <input id="gallery_name" name="gallery_name" type="text" value="<?= $gallery->gallery_name; ?>">
        <label for="gallery_name">Назва галереї</label>
      </div>

      <div class="row">
        <div class="input-field col l10 m12 s12" style="text-align: left; margin-left: 8px;">
          Опис галереї:
        </div>
        <div class="input-field col l10 m12 s12">
          <textarea id="gallery_description" name="gallery_description"><?php echo $gallery->gallery_description; ?></textarea>
          <script>
            CKEDITOR.replace( 'gallery_description' , {
              allowedContent: true
            });
          </script>
        </div>
      </div>

      <div class="row">
        <div class="input-field col l10 m12 s12" style="text-align: left; margin-left: 8px;">
          Додати зображення (оптимальний розмір 900x600, 800x540, 600x400) .jpg:
        </div> 
        <div class="input-field col l10 m12 s12">
          <input id="image_data" name="image[]" type="file" multiple>
        </div>
      </div>

      <input type="hidden" name="action" value="edit_gallery">
      <div class="input-field col l10 m12 s12 center-align">
        <button class="btn waves-effect waves-light" type="submit">
          Змінити<i class="material-icons right">send</i>
        </button>
      </div>
    </form>
  </div>

  <?php if ($gallery->gallery_images):?>
    <div class="card-panel">
      <h5>Зображення</h5>
      <form id="imagesform" action="/admin/gallery/edit/<?= $gallery->gallery_id ?>" method='POST'>
        <table>
          <tr>
            <th>Check</th>
            <th>URL</th>
            <th>Назва</th>
          </tr>

          <?php foreach ($gallery->gallery_images as $key => $image): ?>
            <tr>
              <td>
                <div>
                  <label>
                    <input type="checkbox" id="<?= $image["image"] ?>" name="image[]" value="<?= $image["image"] ?>">
                    <span><img src="<?= $image["small"] ?>" alt="" /></span>
                  </label>
                </div>
              </td>
              <td><?= $image["image_full"] ?></td>
              <td class="image_title_edit">
                <div class="title_view" id="title_view_<?= $image["image_id"] ?>">
                  <strong><?= $image["title"] ?></strong>
                  <a class="btn-floating btn-small waves-effect waves-light" onclick="titleEdit(<?= $image["image_id"] ?>);"><i class="material-icons left">edit</i></a>
                </div>
                <div class="title_edit" id="title_edit_<?= $image["image_id"] ?>">
                  <input type="text">
                  <a class="btn-floating btn-small waves-effect waves-light" onclick="titleSave(<?= $image["image_id"] ?>);"><i class="material-icons left">add</i></a>
                </div>
              </td>
            </tr>
          <?php endforeach; ?>
        </table>
        <input type="hidden" name="action" value="delete_images">
      </form>
      <a class="waves-effect waves-light btn modal-trigger" href="#modal1">Видалити</a>
    </div>
  <?php endif; ?>
</div>

<div id="modal1" class="modal">
  <div class="modal-content">
    <h4>Видалити вибрані зображення?</h4>
  </div>
  <div class="modal-footer">
    <a href="#" class="modal-action modal-close waves-effect waves-green btn-flat" onclick="formSubmit('imagesform')">Видалити</a>
  </div>
</div>
<script>

  $(document).ready(function(){
    $(".title_edit").hide();
    $('.modal').modal();
  });

  function titleEdit(n) {
    var title = $("#title_view_" + n).children("strong").text();
    $("#title_edit_" + n).children("input").val(title);
    $("#title_edit_" + n).show();
    $("#title_edit_" + n).children("input").focus();
    $("#title_view_" + n).hide();
  }

  function titleSave(n){
    var title=$("#title_edit_"+n).children("input").val();
    $.ajax({
      type: 'POST',
      url: '/admin/imageTitleSave/',
      data: ({
        "image_id": n,
        "title": title
      }),
      success: function(d){
        $("#title_view_" + n).children("strong").text(d);
        $("#title_edit_" + n).hide();
        $("#title_view_" + n).show();                 
        Materialize.toast('Заголовок зображення відредаговано!', 1000);
      }
    });
  }

  function formSubmit(f){
    document.getElementById(f).submit();
  }
</script>