<div class="container">
  <div class="card-panel">
    <form class="col s10 offset-s1" action="/admin/galleries/" method="POST">

      <div class="input-field col l10 m12 s12">
        <i class="material-icons prefix">menu</i>
        <input id="gallery_name" name="gallery_name" type="text">
        <label for="gallery_name">Назва галереї</label>
      </div>

      <div class="row">
        <div class="input-field col l10 m12 s12" style="text-align: left; margin-left: 8px;">
          Опис галереї:
        </div>
        <div class="input-field col l10 m12 s12">
          <textarea id="gallery_description" name="gallery_description"></textarea>
          <script>
            CKEDITOR.replace( 'gallery_description' , {
              allowedContent: true
            });
          </script>
        </div>
      </div>


      <input type="hidden" name="action" value="add_gallery">
      <div class="input-field col l10 m12 s12 center-align">
        <button class="btn waves-effect waves-light" type="submit">
          Додати<i class="material-icons right">send</i>
        </button>
      </div>
    </form>
  </div>
</div>
