<div class="container">
  <div class="card-panel">
    <form action="/admin/galleries/" method='POST'>
      <table>
        <tr>
          <th>Check ID галереї</th>
          <th>Назва галереї</th>
          <th>Кількість зображень</th>
        </tr>        
        <?php foreach($galleries as $key => $gallery): ?>
          <tr>
            <td>
              <label>
                <?php if ($gallery['image_count']==0): ?>
                  <input type="checkbox" name="gallery[]" value="<?= $gallery["gallery_id"] ?>">
                <?php else: ?>
                  <input type="checkbox" disabled>
                <?php endif; ?>
                <span><?= $gallery["gallery_id"] ?></span>
              </label>
            </td>
            <td><a href="/admin/gallery/edit/<?= $gallery['gallery_id']; ?>"><span><?= $gallery['gallery_name']; ?></span></a></td>
            <td><?= $gallery['image_count']; ?></td>
          </tr>       
        <?php endforeach; ?>
      </table>
      <input type="hidden" name="action" value="delete_galleries">
      <div class="input-field col l10 m12 s12 center-align">
        <button class="btn waves-effect waves-light" type="submit">
          Видалити<i class="material-icons right">send</i>
        </button>
      </div>
    </form>

  </div>
</div>