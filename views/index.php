<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title><?= $title; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>
    <meta name="keywords" content="<?= $keywords; ?>" />
    <meta name="description" content="<?= $description; ?>" />

    <!-- icons -->
    <link rel="shortcut icon" href="/icons/favicon.ico" type="image/x-icon">
    <link rel="icon" href="/icons/favicon.ico" type="image/x-icon">

    <!-- CSS  -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-beta/css/materialize.min.css">
    <link href="/views/css/style.css" type="text/css" rel="stylesheet">

    <!-- bxslider  -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.css">
    
    <!-- jQuery  -->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.min.js"></script>
    <script src="//cdn.ckeditor.com/4.5.9/full/ckeditor.js"></script>
    
</head>

<body <?php if ($background): ?>style="background-image: url(/images/backgrounds/<?= $background ?>);"<?php endif; ?>>

    <div class="mask"></div>
    <a name="0"></a>
    <?php if ($admin): ?>
        <!-- Admin Panel -->
        <ul id="dropdown-admin-1" class="dropdown-content">
            <li><a href="/admin/categories/">Категорії</a></li>
            <li><a href="/admin/category/add/">Нова категорія</a></li>
        </ul>

        <ul id="dropdown-admin-2" class="dropdown-content">
            <li><a href="/admin/tours/">Тури</a></li>
            <li><a href="/admin/tour/add/">Новий тур</a></li>
        </ul>

        <ul id="dropdown-admin-3" class="dropdown-content">
            <li><a href="/admin/galleries/">Галереї</a></li>
            <li><a href="/admin/gallery/add/">Нова галерея</a></li>
        </ul>
        
        <nav class="grey lighten-3">
            <div class="nav-wrapper">
                <a id="logo-container" href="#" class="brand-logo">Admin Panel</a>
                <ul class="right hide-on-med-and-down">
                    <li><a class="dropdown-trigger" href="#!" data-target="dropdown-admin-1">Категорії<i class="material-icons right">arrow_drop_down</i></a></li>
                    <li><a class="dropdown-trigger" href="#!" data-target="dropdown-admin-2">Тури<i class="material-icons right">arrow_drop_down</i></a></li>
                    <li><a class="dropdown-trigger" href="#!" data-target="dropdown-admin-3">Галереї<i class="material-icons right">arrow_drop_down</i></a></li>
                    <li><a href="/admin/logout/">Вийти</a></li>
                </ul>
                <a href="#" data-target="nav-mobile-admin" class="sidenav-trigger"><i class="material-icons">menu</i></a>
            </div>
        </nav>
        <ul id="nav-mobile-admin" class="sidenav">
            <li><a href="/admin/categories/">Категорії</a></li>
            <li><a href="/admin/categoryadd/">Нова категорія</a></li>
            <li><a href="/admin/tours/">Тури</a></li>
            <li><a href="/admin/touradd/">Новий тур</a></li>
            <li><a href="/admin/galleries/">Галереї</a></li>
            <li><a href="/admin/gallery/add/">Нова галерея</a></li>
            <li><a href="/admin/logout/">Вийти</a></li>
        </ul>
        <?php if (!empty($log)): ?>
        <ul class="collection">
            <?php if (!empty($log['error'])): ?>
                <?php foreach ($log['error'] as $error): ?>
                <li class='collection-item red accent-4 white-text'><?= $error; ?></li>
                <?php endforeach; ?>
            <?php endif; ?>

            <?php if (!empty($log['message'])): ?>
                <?php foreach ($log['message'] as $message): ?>
                <li class='collection-item blue darken-4 white-text'><?= $message; ?></li>
                <?php endforeach; ?>
            <?php endif; ?>
        </ul>
        <?php endif; ?>
        <!-- /Admin Panel -->
    <?php endif; ?>

    <!-- Main Navigation -->
    <ul id="dropdown1" class="dropdown-content">
        <li><a href='/tours'>Усі категорії</a></li>
        <?php foreach($categories as $key => $category): ?>
        <li><a href="/tours/list/<?php echo $category["category_url"]?>"><?php echo $category["category_name"]?></a></li>
        <?php endforeach;?>
    </ul>
    
    <nav class="main" <?php if ($backgroundHeader):?>style="background-image: url(/images/backgrounds/<?= $backgroundHeader ?>);"<?php endif; ?>>
        <div class="nav-wrapper container">
            <a id="logo-container" href="/" class="brand-logo"><img src="/images/logo.png"></a>
            <a href="#" data-target="nav-mobile" class="sidenav-trigger"><i class="material-icons blue-text text-darken-4">menu</i></a>
            <h2 class="logo-text">Туристичний оператор</h2>
            <ul class="right hide-on-med-and-down">
                <li><a class="white-text" href="/">Головна</a></li>
                <li><a class="dropdown-trigger white-text" href="#!" data-target="dropdown1">Наші тури<i class="material-icons right">arrow_drop_down</i></a></li>
                <li><a class='white-text' href="/book/">Пошук та бронювання турів</a></li>

            </ul>
        </div>
    </nav>
    <ul id="nav-mobile" class="sidenav">
      <li><a href="/">Головна</a></li>
      <li><a href="/tours/categories/">Наші тури</a></li>
      <li><a href="/book/">Пошук та бронювання турів</a></li>
    </ul>
    <!-- /Main Navigation -->

  <?php echo $content; ?>

  <footer class="page-footer blue darken-1">
    <div class="container">
      <div class="row">

        <div class="col s12 m6 l4">
          <h5 class="white-text">"Туристичний оператор"</h5>
          <p class="grey-text text-lighten-4">Веб-система</p>
        </div>
        <div class="col s12 m6 l4">
          <h5 class="white-text">Контакти</h5>
          <ul>
            <li class='white-text'>Chudnivska St, 103, Zhytomyr, Zhytomyrs'ka oblast, 10002</li>
            <li class="white-text"><strong>Email:</strong> <a class="white-text" href="mailto:admin@admin">admin@touroperator.com</a></li>
            <li class="white-text"><strong>Skype:</strong> tour_operator</li>
          </ul>
        </div>
        <div class="col s12 m6 l4">
          <h5 class="white-text">Телефони</h5>
          <ul>
            <li class='white-text'>(0888) 88-88-88 (10.00-16.00)</li>
            <li class='white-text'>11-11-11 (10.00-20.00)</li>
            <li class='white-text'>012345678; 012345678</li>
            <li class='white-text'>012345678; 012345678</li>
          </ul>
        </div>
      </div>
    </div>
    <div class="footer-copyright blue darken-2">
      <div class="container">
        <a class="white-text" href="http://touroperator/">&copy; Туристичний оператор</a>
      </div>
    </div>
  </footer>

    
   
    <!-- Materialize  -->
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-beta/js/materialize.min.js"></script>
    
    <script type="text/javascript">

    $(document).ready(function(){
        $('.slider').slider({full_width: true});
        $(".dropdown-trigger").dropdown();
        $('.tabs').tabs();
        $('.sidenav').sidenav();
        $('select').formSelect();
    });

    $('.datepicker').pickadate({
     selectMonths: true, // Creates a dropdown to control month
     selectYears: 15, // Creates a dropdown of 15 years to control year,
     today: 'Today',
     clear: 'Clear',
     close: 'Ok',
     closeOnSelect: false // Close upon selecting a date,
    });

    </script>

</body>
</html>