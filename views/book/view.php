<div class="container">
  <div class="row">
    <div class="col s12 l6">
      <div class="card-panel">
        <h4>Заявка на пошук туру за кордоном</h4>
        <div class="row">
          <form method="post" action="https://formspree.io/tury.po.ukraineZT@gmail.com" class="col s12">
            <div class="row">
              <div class="input-field col s4">
                <i class="material-icons prefix">account_circle</i>
                <input id="last_name" name="last_name" type="text" class="validate" placeholder="Last Name" required="required">
                <label for="last_name">Прізвище</label>
              </div>
              <div class="input-field col s4">
                <input id="first_name" name="first_name" type="text" class="validate" placeholder="First Name" required="required">
                <label for="first_name">Ім'я</label>
              </div>
              <div class="input-field col s4">
                <input id="patronimic" name="patronimic" type="text" class="validate" placeholder="Patronimic" required="required">
                <label for="patronimic">По батькові</label>
              </div>
            </div>
            <div class="row">
              <div class="input-field col s12">
                <i class="material-icons prefix">phone</i>
                <input id="phone_number" name="phone_number" type="tel" class="validate" placeholder="Phone Number" required="required">
                <label for="phone_number">Контактний телефон</label>
              </div>
            </div>
            <div class="row">
              <?php foreach ($countries as $country): ?>
                <div class="col s4">
                  <input name="country" type="radio" id="country_<?= $country['code'] ?>" value="<?= $country['code'] ?>" />
                  <label for="country_<?= $country['code'] ?>"><img src="/images/flags/<?= strtolower($country['code']) ?>.png"> <?= $country['name_ua'] ?></label>
                </div>
              <?php endforeach; ?>

            </div>
            <div class="row">
              <div class="input-field col s12">
                <i class="material-icons prefix">work</i>
                <select name="hotel_category" id="category">
                  <option value="" disabled selected>Виберіть категорію</option>
                  <option value="2">2*</option>
                  <option value="3">3*</option>
                  <option value="4">4*</option>
                  <option value="5_HV1">5*/HV1</option>
                  <option value="Boutique">Boutique</option>
                </select>
                <label for="category">Категорія готелю</label>
              </div>
            </div>
            <div class="row">
              <div class="input-field col s12">
                <i class="material-icons prefix">local_dining</i>
                <select name="hotel_meals" id="meals">
                  <option value="" disabled selected>Харчування</option>
                  <option value="all_inclusive">ALL INCLUSIVE</option>
                  <option value="bed_and_breakfast">BED & BREAKFAST</option>
                  <option value="full_board">FULL BOARD</option>
                  <option value="half_board">HALF BOARD</option>
                  <option value="ultra_ai">ULTRA AI</option>
                </select>
                <label for="meals">Харчування</label>
              </div>              
            </div>
            <div class="row">
              <div class="input-field col s4">
                <i class="material-icons prefix">people</i>
                <input id="adult_number" name="adult_number" type="number" value="2" min="1" class="validate" placeholder="Adult Number" >
                <label for="adult_number">Кількість дорослих</label>
              </div>

              <div class="input-field col s4">
                <i class="material-icons prefix">child_care</i>
                <input id="children_number" name="children_number" type="number" value="0" min="0" class="validate" placeholder="Children Number" >
                <label for="children_number">Кількість дітей</label>
              </div> 
              <div class="input-field col s4">
                <input id="children_age" name="children_age" type="text" class="validate" placeholder="Children Age" >
                <label for="children_age">Вік дітей</label>
              </div>  
            </div>
            <div class="row">
              <div class="input-field col s6">
                <i class="material-icons prefix">date_range</i>
                <label for="date_begin">Виліт від</label>
                <input id="date_begin" name="date_begin" type="text" class="validate datepicker">

              </div>
              <div class="input-field col s6">
                <label for="date_end">До</label>
                <input id="date_end" name="date_end" type="text" class="validate datepicker">
              </div>
            </div>
            <div class="row">
              <div class="input-field col s4">
                <i class="material-icons prefix">attach_money</i>
                <select name="currency" id="currency">
                  <option value="USD">USD</option>
                  <option value="EUR">EUR</option>
                  <option value="UAH">UAH</option>
                </select>
                <label for="currency">Ціна</label>
              </div>              

              <div class="input-field col s4">
                <input id="price_begin" name="price_begin" type="number" value="0" min="0" class="validate">
                <label for="price_begin">Від</label>
              </div>

              <div class="input-field col s4">
                <input id="price_end" name="price_end" type="number" value="0" min="0" class="validate">
                <label for="price_end">До</label>
              </div> 
            </div>
            <button class="btn waves-effect waves-light" type="submit" name="action">Надіслати
              <i class="material-icons right">send</i>
            </button>
          </form>
        </div>
      </div>         
    </div>

    <div class="col s12 l6">
      <div class="card-panel">
        <h4>Заявка на бронь туру по Україні</h4>
        <div class="row">
          <form method="post" action="https://formspree.io/tury.po.ukraineZT@gmail.com"  class="col s12">
            <div class="row">
              <div class="input-field col s12">
                <i class="material-icons prefix">location_on</i>
                <input id="tour_name" name="tour_name" type="text" class="validate" placeholder="Tour Name" required="required">
                <label for="tour_name">Назва туру</label>
              </div>
            </div>
            <div class="row">
              <div class="input-field col s12">
                <i class="material-icons prefix">date_range</i>
                <input id="date_begin" name="date_begin" type="text" class="validate datepicker" required="required">
                <label for="date_begin">Дата виїзду</label>
              </div>
            </div>
            <div class="row">
              <div class="input-field col s4">
                <i class="material-icons prefix">account_circle</i>
                <input id="last_name" name="last_name" type="text" class="validate" placeholder="Last Name" required="required">
                <label for="last_name">Прізвище</label>
              </div>
              <div class="input-field col s4">
                <input id="first_name" name="first_name" type="text" class="validate" placeholder="First Name" required="required">
                <label for="first_name">Ім'я</label>
              </div>
              <div class="input-field col s4">
                <input id="patronimic" name="patronimic" type="text" class="validate" placeholder="Patronimic" >
                <label for="patronimic">По батькові</label>
              </div>
            </div>
            <div class="row">
              <div class="input-field col s4">
                <i class="material-icons prefix">date_range</i>
                <select id="birth_day" name="birth_day">
                  <?php for ($i = 1; $i <= 31; $i++): ?>
                    <option value="<?= $i ?>"><?= ($i < 10) ? 0 : '' ?><?= $i ?></option>
                  <?php endfor; ?>
                </select>
                <label for="day_of_birth">Дата народження</label>
              </div>
              <div class="input-field col s4">
                <select id="birth_month" name="birth_month">
                  <?php foreach ($months as $key => $month): ?>
                    <option value="<?= $key ?>"><?= $month ?></option>
                  <?php endforeach; ?>
                </select>
                <label for="day_of_birth">Місяць</label>
              </div>
              <div class="input-field col s4">
                <select id="birth_year" name="birth_year">
                  <?php for ($i = 2017; $i >= 1910; $i--): ?>
                    <option value="<?= $i ?>"><?= $i ?></option>
                  <?php endfor; ?>
                </select>
                <label for="day_of_birth">Рік</label>
              </div>
            </div>
            <div class="row">
              <div class="input-field col s12">
                <i class="material-icons prefix">phone</i>
                <input id="phone_number" name="phone_number" type="tel" class="validate" placeholder="Phone Number" >
                <label for="phone_number">Контактний телефон</label>
              </div>
            </div>
            <button class="btn waves-effect waves-light" type="submit" name="action">Надіслати
              <i class="material-icons right">send</i>
            </button>
          </form>
        </div>
      </div> 
    </div>
  </div>

</div>
<script>
  $('.datepicker').pickadate({
selectMonths: true,//Creates a dropdown to control month
selectYears: 15,//Creates a dropdown of 15 years to control year
//The title label to use for the month nav buttons
labelMonthNext: 'Наступний місяць',
labelMonthPrev: 'Попередній місяць',
//The title label to use for the dropdown selectors
labelMonthSelect: 'Виберіть місяць',
labelYearSelect: 'Виберіть рік',
//Months and weekdays
monthsFull: [ 'Січень', 'Лютий', 'Березень', 'Квітень', 'Травень', 'Червень', 'Липень', 'Серпень', 'Вересень', 'Жовтень', 'Листопад', 'Грудень' ],
monthsShort: [ 'Січ', 'Лют', 'Бер', 'Квіт', 'Трав', 'Черв', 'Лип', 'Серп', 'Вер', 'Жовт', 'Лист', 'Груд' ],
weekdaysFull: [ 'Неділя', 'Понеділок', 'Вівторок', 'Середа', 'Четвер', 'П\'ятниця', 'Субота' ],
weekdaysShort: [ 'НД', 'ПН', 'ВТ', 'СР', 'ЧТ', 'ПТ', 'СБ' ],
//Materialize modified
weekdaysLetter: [ 'Н', 'П', 'В', 'С', 'Ч', 'П', 'С' ],
//Today and clear
today: 'Сьогодні',
clear: 'Очистити',
close: 'Закрити',
//The format to show on the `input` element
format: 'dd/mm/yyyy'
}); 
</script>