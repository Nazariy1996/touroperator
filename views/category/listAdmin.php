<div class="container">   
  <div class="card-panel">    

    <table>
      <tr>
        <th>ID категорії</th>
        <th>Назва категорії</th>
        <th>Батьківська категорія</th>
        <th>На головній</th>
      </tr>        
      <?php foreach($categories as $key => $category): ?>
        <tr>
          <td><?= $category['category_id']; ?></td>
          <td>
            <a href="/admin/category/edit/<?= $category['category_id']; ?>">
              <span <?php if ($category["is_active"]==0) echo "class='red-text line-through'" ?>>
                <?= $category['category_name']; ?>
              </span>
            </a>
          </td>
          <td><?= $category['parent']; ?></td>
          <td><?= $category['is_main']; ?></td>
        </tr>       
      <?php endforeach; ?>
    </table>
  </div>
</div>
