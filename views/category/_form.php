<div class="container">  
  <div class="card-panel">
    <form class="col s10 offset-s1" action="/admin/categories" method="POST">

      <div class="input-field col l10 m12 s12">
        <i class="material-icons prefix">menu</i>
        <input id="category_id" name="category_id" type="text" readonly="readonly" value="<?= $category->category_id; ?>">
        <label for="category_id">ID категорії</label>
      </div>

      <div class="input-field col l10 m12 s12">
        <i class="material-icons prefix">menu</i>
        <input id="category_name" name="category_name" type="text" required="required" value="<?php echo $category->category_name; ?>">
        <label for="category_name">Назва</label>
      </div>

      <div>
        <label for="parent_id">Батьківська категорія</label>
        <select name="parent_id">
          <option value="0">Нуль-категорія</option>
          <?php foreach($parents as $key => $parent): ?>
            <?php if ($category->parent_id == $parent["category_id"]): ?>
              <option value="<?= $parent["category_id"]; ?>" selected><?= $parent["category_name"]; ?></option>
            <?php else: ?>
              <option value="<?= $parent["category_id"]; ?>"><?= $parent["category_name"]; ?></option>
            <?php endif; ?>
          <?php endforeach; ?>
        </select>
      </div>

      <div class="row">
        <div class="input-field col l10 m12 s12" style="text-align: left; margin-left: 8px;">
          <label for="category_description">Короткий опис:</label>
        </div>
        <div class="input-field col l10 m12 s12" style="margin: 50px 50px;">
          <textarea id="category_description" name="category_description"><?php echo $category->category_description; ?></textarea>
          <script>
            CKEDITOR.replace( 'category_description' , {
              allowedContent: true
            });
          </script>
        </div>
      </div>

      <div class="row">
        <div class="input-field col l10 m12 s12" style="text-align: left; margin-left: 8px;">
          <label for="category_long_description">Повний опис:</label>
        </div>
        <div class="input-field col l10 m12 s12" style="margin: 50px 50px;">
          <textarea id="category_long_description" name="category_long_description"><?php echo $category->category_long_description; ?></textarea>
          <script>
            CKEDITOR.replace( 'category_long_description' , {
              allowedContent: true
            });
          </script>
        </div>
      </div>

      <div class="input-field col l10 m12 s12">
        <i class="material-icons prefix">menu</i>
        <input id="category_url" name="category_url" type="text" required="required" value="<?php echo $category->category_url; ?>">
        <label for="category_url">URL категорії</label>
      </div>

      <div class="col l10 m12 s12">
        <label>
          <input id="is_active" name="is_active" type="checkbox" value='1' <?php if ($category->is_active == 1): ?>checked<?php endif; ?> >
          <span>Активний</span>
        </label>
      </div>

      <div class="col l10 m12 s12">
        <label>
          <input id="is_main" name="is_main" type="checkbox" value='1' <?php if ($category->is_main == 1): ?>checked<?php endif; ?> >
          <span>На головній</span>
        </label>
      </div>

      <div class="input-field col l10 m12 s12 center-align">
        <?php if ($category): ?>                       
          <input type="hidden" name="action" value="update_category">
          <button class="btn waves-effect waves-light" type="submit">
            Змінити<i class="material-icons right">send</i>
          </button>
        <?php else: ?>
          <input type="hidden" name="action" value="add_category">
          <button class="btn waves-effect waves-light" type="submit">
            Додати<i class="material-icons right">send</i>
          </button>
        <?php endif; ?> 
      </div>

    </form>
  </div>
</div>
