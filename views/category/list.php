<div class="container">
  <nav class="breadcrumbs">
    <div class="nav-wrapper">
      <div class="col s12">
        <?php foreach($breadcrumbs as $key => $item): ?>
          <a href="<?= $item["url"]; ?>" class="breadcrumb"><?= $item["title"]; ?></a>
        <?php endforeach; ?>
      </div>
    </div>
  </nav>

  <div class="row">
    <div class="col s12">
      <div class="card-panel" id="our-tours">
        <h4>Наші тури</h4>
        <div class='collection'>
          <?php foreach($categories as $key => $category): ?>
            <a class="collection-item avatar" href="/tours/list/<?= $category["category_url"]?>">
              <img src="/images/pictograms/<?= $category["category_id"]?>.png" class='circle'>
              <span class='title valign-wrapper'><?= $category["category_name"]?></span>
            </a>
          <?php endforeach;?>
        </div>
      </div>
    </div>
  </div>
</div>