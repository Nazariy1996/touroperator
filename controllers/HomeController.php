<?php
class HomeController extends Controller
{
    const ADMIN_NAME = 'admin';
    const ADMIN_PASSWORD = 'admin';
    
    public function actionIndex($parameters = []) 
    {
        $params['busGalleries'] = (new Tour())->getGalleries(1);
        $params['categories'] = $this->categories;
        $params['menu'] = Category::getCategoriesMenu();
        $this->title = "Тури по Україні Житомир";
        $this->render('home', $params);
    }  

    
    public function actionLogin($parameters = [])
    {
        if (self::checkLogin()) {
            header('Location: /admin/tours/');
            die;
        }
        $this->render('login');
    }

    public function actionLogout()
    {
        session_destroy();
        header('Location: /admin/login');
        die;
    }
    
    public function actionPage404($parameters = [])
    {
        $this->render('page404');
    }
    
    protected function checkLogin() 
    {
        if ($_SESSION['admin']) {
            return true;
        }
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            if ($_POST['name'] === self::ADMIN_NAME &&
                $_POST['password'] === self::ADMIN_PASSWORD) {
                $_SESSION['admin'] = true;
                $this->admin = true;
                return true;
            } else {
                $this->log['errors'][] = 'Неуспішний вхід. Спробуйте ще раз.';
            }
        }
        return false;
    }

}