<?php

class BookController extends Controller
{
    public function actionView($parameters = [])
    {
        $params['countries'] = Country::getCountries();
        $this->render('view', $params);
    }
}