<?php

class CategoryController extends Controller
{
    public function actionListAdmin($parameters = [])
    {
         if (isset($_POST['action'])) {
            try {
                $category = new Category();
                $category->initObjectFromArray($_POST);
                if ($_POST['action'] == 'update_category') {
                    if($category->update()) {
                        $this->log['message'][] = "Дані про категорію успішно змінено";
                    } else {
                        $this->log['error'][] = "Неможливо змінити дані про категорію";
                    }
                } else if($_POST['action'] == 'add_category') {
                    if ($category->create()) {
                        $this->log['message'][]="Нову категорію додано";
                    }
                }
            } catch (Exception $e) {
                $this->log['error'][] = $e->getMessage();
            }
        }
        
        $params['categories'] = Category::getCategoriesAdmin();
        $category_id = ($_POST['category_id']) ? $_POST['category_id'] : 0;
        $params['category_id'] = $category_id;
        $this->render('listAdmin', $params);
    }    

    public function actionList($parameters = [])
    {
        $params['categories'] = $this->categories;
        $params['breadcrumbs'] = [
            ['url'=>'/', 'title'=>'Головна'],
            ['url'=>'/tours', 'title'=>'Наші тури'],
        ];
        $this->title = "Наші тури";
        $this->render('list', $params);        
    }
    
    public function actionEdit($parameters = [])
    {
        $category_id = $parameters[0];
        $category = new Category($category_id);
        $params['category'] = $category;
        $params['parents'] = Category::getCategoriesByParent(0);
        $this->render('_form', $params);
    }

    public function actionAdd($parameters = [])
    {
        $params['parents'] = Category::getCategoriesByParent(0);
        $this->render('_form', $params);
    }
}