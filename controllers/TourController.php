<?php
class TourController extends Controller
{
    
    public function actionList($parameters = []) 
    {
        $category_url = $parameters[0];
        $tour_length_id = ($_POST["tour_length_id"]) ? $_POST["tour_length_id"] : 0;
        $category = new Category(0, $category_url);
        $breadcrumbs = [
                0 => ['url' => '/', 'title' => 'Головна'],
                1 => ['url' => '/tours', 'title' => 'Наші тури'],
                2 => ['url' => '/tours/list/' . $category_url, 'title' => $category->category_name],
            ];
        $params['category'] = $category;
        $params['breadcrumbs'] = $breadcrumbs;
        $params['tours'] = Tour::getToursByCategory($category->category_id, $tour_length_id);
        $params['tour_lengths'] = Tour::getTourLengths();
        $params['tour_length_id'] = $tour_length_id;
        $params['menu'] = Category::getCategoriesMenu();
        $this->title = $category->category_name;
        $this->background = $category->background_image;
        $this->backgroundHeader = $category->background_image_header;
        $this->render('list', $params);
    }

    /**
     * Displays full info about Tour
     * Modified 08.07.2017 by NazarKrobust
     */    
    public function actionView($parameters = []) 
    {
        $category_id = $parameters[0];
        $tour_id = $parameters[1];
        $tour = new Tour($tour_id);
        $category = new Category($category_id);
        $tour->galleries = $tour->getGalleries($tour_id);
        $params['tour'] = $tour;
        $params['category'] = $category;
        $breadcrumbs = [];
        $breadcrumbs[0] = ['url' => '/', 'title' => 'Головна'];
        $breadcrumbs[1] = ['url' => '/tours', 'title' => 'Наші тури'];
        $breadcrumbs[2] = ['url' => '/tours/list/' . $category->category_url, 'title' => $category->category_name];
        $breadcrumbs[3] = ['url' => '/tours/view/' . $category->category_id . '/' . $tour->tour_id, 'title' => $tour->tour_name];
        $params['breadcrumbs'] = $breadcrumbs;
        $params['categories'] = $this->categories;
        $params['background'] = $tour->background_image;
        $params['background_header'] = $tour->background_image_header;
        $this->title = $tour->meta_title;
        $this->description = $tour->meta_description;
        $this->keywords = $tour->meta_keywords;
        $this->render('view', $params);
    }
    
    /**
     * Displays the list of Tours for administration
     * with category, length, isActive filters;
     * Handles the requests for updating and creating a tour
     * Modified 08.07.2017 by NazarKrobust
     */
    public function actionListAdmin($parameters = [])
    { 
        if (isset($_POST['action'])) {
            try {
                $tour = new Tour();
                $tour->initObjectFromArray($_POST);
                $tour->setGalleryIds($_POST['tour_gallery']);
                $tour->setCategoryIDs($_POST['tour_category']);
                if($_POST['action'] == 'update_tour') {
                    if($tour->update()) {
                        $this->log['message'][]="Дані про тур успішно змінено.";
                        $event_success = true;
                    } else {
                        $this->log['error'][] = "Неможливо змінити дані про тур";
                    }
                } elseif ($_POST['action'] == 'add_tour') {
                    if ($tour->create()) {
                        $this->log['message'][] = "Новий тур додано";
                        $event_success = true;  
                    }
                }
            } catch (Exception $e) {
                $this->log['error'][] = $e->getMessage();
            }
        }
        $category_id = ($_POST['category_id']) ? $_POST['category_id'] : 0;
        $tour_length_id = ($_POST['tour_length_id']) ? $_POST['tour_length_id'] : 0;
        $is_active = (isset($_POST['is_active'])) ? $_POST['is_active'] : 1;
        if ($category_id > 0) {
          $params['tours'] = Tour::getToursByCategory($category_id, $tour_length_id, $is_active);
        } else {
          $params['tours'] = Tour::getTours($tour_length_id, $is_active);
        }
        
        
        $params['categories'] = $this->categories;
        $params['tour_lengths'] = Tour::getTourLengths();        
        $params['category_id'] = $category_id;
        $params['tour_length_id'] = $tour_length_id;
        $params['is_active'] = $is_active;
        $this->render('list_admin', $params);
    }
    
    public function actionEdit($parameters = [])
    {
        $tour_id = $parameters[0];
        $tour = new Tour($tour_id);
        $params['tour'] = $tour;
        $params['categories'] = $this->categories;
        $params['tour_lengths'] = Tour::getTourLengths();
        $params['galleries'] = Gallery::getGalleries();
        $this->render('_form', $params);
    }

    public function actionAdd($parameters = [])
    {
        $params['categories'] = Category::getCategoriesAdmin();
        $params['tour_lengths'] = Tour::getTourLengths();
        $params['galleries'] = Gallery::getGalleries();
        $this->render('_form', $params);
    }
    
}