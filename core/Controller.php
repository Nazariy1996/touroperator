<?php
class Controller 
{   
    protected $controllerID;
    protected $actionID;
    protected $view;
    protected $content;
    protected $title;
    protected $description;
    protected $keywords;
    protected $background;
    protected $backgroundHeader;
    protected $log;
    protected $categories;
    protected $admin;
    
    public function __construct($controllerID, $actionID, $admin) 
    {
        $this->controllerID = $controllerID;
        $this->actionID = $actionID;
        $this->admin = $admin;
        $this->admin = (isset($_SESSION['admin'])) ? $_SESSION['admin'] : false;
        $this->categories = Category::getCategories();

    }
    
    public function render($template, $params = [])
    {
        $this->view = new View('index');
        $this->content = (new View($this->controllerID . '/' . $template, $params))->getHTML(); 
        $this->view->setParam('content', $this->content);
        $this->view->setParam('admin', $this->admin);
        $this->view->setParam('title', $this->title);
        $this->view->setParam('background', $this->background);
        $this->view->setParam('backgroundHeader', $this->backgroundHeader);
        $this->view->setParam('log', $this->log);
        $this->view->setParam('categories', $this->categories);
        
        $this->view->render();
    }
}
