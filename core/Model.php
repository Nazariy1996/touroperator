<?php

class Model 
{
    public function initObjectFromArray($array = []) 
    {
        foreach ($array as $property => $value) {
            $this->$property = $value;
        }
    }
    
    public function setObjectToSession($name)
    {
        foreach ($this as $property => $value) {
            $_SESSION[$name][$property] = $value;
        }
    }

}
