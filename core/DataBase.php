<?php
class DataBase {
    protected static $_database;

    public static function Connect($host = DB_HOSTNAME, $paramsbase = DB_DATABASE, 
            $user = DB_USERNAME, $password = DB_PASSWORD, $port = DB_PORT)
    {
        try 
        {
            $dsn = "mysql:host=$host;port=$port;dbname=$paramsbase;charset=utf8";
            self::$_database = new PDO($dsn, $user, $password);
            self::$_database->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            self::$_database->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
        } catch (PDOException $e) {
            echo "Connection failed: " . $e->getMessage();
            die;
        }
    }

    public static function handler() 
    {
        return self::$_database;
    }
}