<?php
// HTTP
define('HTTP_SERVER', 'localhost');

// HTTPS
define('HTTPS_SERVER', 'http://touroperator/');

define('EMAIL_ADDRESS', 'admin@touroperator');

define('SITE_NAME', 'Туристичний оператор');
// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_DATABASE', 'touroperator');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_PORT', '');