<?php
return [
    ""                              => "home/index",
    "page404"                       => "home/page404",
    
    "tours"                         => "category/list",
    "admin/categories"              => "category/listAdmin",
    "admin/category/edit/([0-9]+)"  => "category/edit/$1",
    "admin/category/add"            => "category/add",    
    
    
    "tours/list/([\w\-]+)"          => "tour/list/$1",
    "tour/([0-9]+)/([0-9]+)"        => "tour/view/$1/$2",
    "admin/tours"                   => "tour/listAdmin",
    "admin/tour/edit/([0-9]+)"      => "tour/edit/$1",
    "admin/tour/add"                => "tour/add",  
    
    "admin"                         => "home/login",
    "admin/login"                   => "home/login",
    "admin/logout"                  => "home/logout",
    
    "admin/galleries"               => "gallery/list",
    "admin/gallery/edit/([0-9]+)"   => "gallery/edit/$1",
    "admin/gallery/add"             => "gallery/add",
    "admin/imageTitleSave"          => "gallery/imageTitleSave",
    
    "book"                          => "book/view",
];
